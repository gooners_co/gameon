//
//  SignUpViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 10/26/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var firstName:UITextField!
    @IBOutlet var lastName:UITextField!
    @IBOutlet var email:UITextField!
    @IBOutlet var username:UITextField!
    @IBOutlet var password:UITextField!
    @IBOutlet var confirmPassword:UITextField!
    @IBOutlet var submitButton:UIButton!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.email.delegate = self
        self.username.delegate = self
        self.password.delegate = self
        self.confirmPassword.delegate = self
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: Selector("resignKeyboard"))
        self.view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func submitForm(sender:UIButton) {
        self.submitButton.enabled = false
        if (self.firstName.text == "" || self.lastName.text == "" || self.email.text == "" || self.username.text == "" || self.password.text == "" || self.confirmPassword.text == "") {
            let alert = UIAlertController(title: "Woops", message: "You must fill out all fields before submitting.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            self.submitButton.enabled = true
            return
        }
        else if (self.password.text != self.confirmPassword.text) {
            let alert = UIAlertController(title: "Woops", message: "Your passwords do not match.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            self.submitButton.enabled = true
            return
        }
        
        let pfuser = PFUser()
        pfuser.username = self.username.text!
        pfuser.password = self.password.text!
        pfuser.email = self.email.text!
        pfuser["firstName"] = self.firstName.text!
        pfuser["lastName"] = self.lastName.text!
        pfuser.signUpInBackgroundWithBlock { (succeeded:Bool, error:NSError?) -> Void in
            if let error = error {
                let errorString = error.userInfo["error"] as? NSString
                print("Error: \(errorString)")
                self.submitButton.enabled = true
            }
            else {
                print("Successfully created user.")
                self.submitButton.enabled = true
                self.performSegueWithIdentifier("YourSkillsViewControllerSegue", sender: nil)
            }
        }
    }
    
    func resignKeyboard() {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == self.firstName) {
            self.firstName.resignFirstResponder()
            self.lastName.becomeFirstResponder()
        }
        else if (textField == self.lastName) {
            self.lastName.resignFirstResponder()
            self.email.becomeFirstResponder()
        }
        else if (textField == self.email) {
            self.email.resignFirstResponder()
            self.username.becomeFirstResponder()
        }
        else if (textField == self.username) {
            self.username.resignFirstResponder()
            self.password.becomeFirstResponder()
        }
        else if (textField == self.password) {
            self.password.resignFirstResponder()
            self.confirmPassword.becomeFirstResponder()
        }
        else if (textField == self.confirmPassword) {
            self.confirmPassword.resignFirstResponder()
            self.submitForm(UIButton.init())
        }
        
        return true
    }
    

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }

}






