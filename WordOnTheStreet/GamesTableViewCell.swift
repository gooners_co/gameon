//
//  GamesTableViewCell.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 11/11/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit

class GamesTableViewCell: UITableViewCell {
    @IBOutlet var gameImage:UIImageView!
    @IBOutlet var gameType:UILabel!
    @IBOutlet var gameTime:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
