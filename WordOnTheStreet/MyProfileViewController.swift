//
//  MyProfileViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 10/23/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import Parse
import CoreImage

class MyProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var profilePic:UIImageView!
    @IBOutlet var photosOptionView:UIView!
    @IBOutlet var usernameLabel:UILabel!
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var ratingsView:UIView!
    @IBOutlet var gamesView:UIView!
    @IBOutlet var gamesTable:UITableView!
    @IBOutlet var ratingsButton:UIButton!
    @IBOutlet var gamesButton:UIButton!
    @IBOutlet var baseballSkillView:UIImageView!
    @IBOutlet var basketballSkillView:UIImageView!
    @IBOutlet var footballSkillView:UIImageView!
    @IBOutlet var soccerSkillView:UIImageView!
    @IBOutlet var volleyballSkillView:UIImageView!
    @IBOutlet var addFriendButton:UIButton!
    @IBOutlet var logOutButton:UIButton!
    @IBOutlet var num_friendsButton:UIButton!
    @IBOutlet var num_gamesButton:UIButton!
    
    var user = PFUser()
    var username:String = String()
    var name:String = String()
    var profileImage:UIImage = UIImage(named: "TapToAdd")!
    var baseballSkill:String = String()
    var basketballSkill:String = String()
    var footballSkill:String = String()
    var soccerSkill:String = String()
    var volleyballSkill:String = String()
    
    let picker = UIImagePickerController()
    var gameIDs = NSArray()
    var friends = NSMutableArray()
    var gameTitles = ["Baseball", "Basketball", "Football", "Soccer", "Volleyball"]
    let skillLevels = ["1, Noob", "2, Play for Fun", "3, Average", "4, Athletic", "5, Skilled"]
    
    var tempTitle = String()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.usernameLabel.text = username
        self.nameLabel.text = name
        if (self.profileImage.isEqual(UIImage(named: "TapToAdd"))) {
            let userImageFile = self.user["profilePic"]
            if (userImageFile != nil) {
                userImageFile.getDataInBackgroundWithBlock({ (imageData:NSData?, error:NSError?) -> Void in
                    if (error == nil) {
                        self.profileImage = UIImage(data: imageData!)!
                        self.profilePic.image = self.profileImage
                    }
                })
            }
        }
        else {
            self.profilePic.image = profileImage
        }
        self.performSelectorOnMainThread(Selector("loadSkills"), withObject:nil, waitUntilDone: true)
        
        picker.delegate = self
        self.gamesTable.delegate = self
        self.gamesTable.dataSource = self

        self.profilePic.layer.cornerRadius = 50.0
        self.profilePic.clipsToBounds = true
        
        let picTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("changeProfilePic"))
        self.profilePic.addGestureRecognizer(picTap)
        
        self.photosOptionView.alpha = 0
        self.ratingsView.backgroundColor = UIColor(white: 1, alpha: 1)
        self.ratingsView.alpha = 1
        self.gamesView.alpha = 0
        
        if (self.user == PFUser.currentUser()) {
            self.addFriendButton.alpha = 0
            self.logOutButton.alpha = 1
        }
        else {
            self.addFriendButton.alpha = 1
            self.logOutButton.alpha = 0
        }
        
        self.num_gamesButton.setTitle("\(self.gameIDs.count)", forState: UIControlState.Normal)
        self.num_friendsButton.setTitle("\(self.friends.count)", forState: UIControlState.Normal)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let array = self.user["friends"] as? NSMutableArray
        if (array != nil) {
            self.friends = array!
            self.num_friendsButton.setTitle("\(self.friends.count)", forState: UIControlState.Normal)
        }
    }
    
    func loadSkills() {
        print("gameIDs count: \(self.gameIDs.count)")
        
        if (baseballSkill == "" || basketballSkill == "" || footballSkill == "" || soccerSkill == "" || volleyballSkill == "") {
            print("User's skills were nil after segue, attempting to reload")
            let query = PFQuery(className: "UserSkills")
            query.whereKey("user", equalTo: self.user.objectId!)
            let objects:[PFObject]
            do {
                objects = try query.findObjects()
                let skills = objects.last
                baseballSkill = skills!["baseballSkill"] as! String
                basketballSkill = skills!["basketballSkill"] as! String
                footballSkill = skills!["footballSkill"] as! String
                soccerSkill = skills!["soccerSkill"] as! String
                volleyballSkill = skills!["volleyballSkill"] as! String
            } catch _ {
                print("An error occured trying to load the data.")
            }
        }
        
        if (baseballSkill.containsString("1")) {
            baseballSkillView.image = UIImage(named: "SkillLevel1")
        }
        else if (baseballSkill.containsString("2")) {
            baseballSkillView.image = UIImage(named: "SkillLevel2")
        }
        else if (baseballSkill.containsString("3")) {
            baseballSkillView.image = UIImage(named: "SkillLevel3")
        }
        else if (baseballSkill.containsString("4")) {
            baseballSkillView.image = UIImage(named: "SkillLevel4")
        }
        else if (baseballSkill.containsString("5")) {
            baseballSkillView.image = UIImage(named: "SkillLevel5")
        }
        
        if (basketballSkill.containsString("1")) {
            basketballSkillView.image = UIImage(named: "SkillLevel1")
        }
        else if (basketballSkill.containsString("2")) {
            basketballSkillView.image = UIImage(named: "SkillLevel2")
        }
        else if (basketballSkill.containsString("3")) {
            basketballSkillView.image = UIImage(named: "SkillLevel3")
        }
        else if (basketballSkill.containsString("4")) {
            basketballSkillView.image = UIImage(named: "SkillLevel4")
        }
        else if (basketballSkill.containsString("5")) {
            basketballSkillView.image = UIImage(named: "SkillLevel5")
        }
        
        if (footballSkill.containsString("1")) {
            footballSkillView.image = UIImage(named: "SkillLevel1")
        }
        else if (footballSkill.containsString("2")) {
            footballSkillView.image = UIImage(named: "SkillLevel2")
        }
        else if (footballSkill.containsString("3")) {
            footballSkillView.image = UIImage(named: "SkillLevel3")
        }
        else if (footballSkill.containsString("4")) {
            footballSkillView.image = UIImage(named: "SkillLevel4")
        }
        else if (footballSkill.containsString("5")) {
            footballSkillView.image = UIImage(named: "SkillLevel5")
        }
        
        if (soccerSkill.containsString("1")) {
            soccerSkillView.image = UIImage(named: "SkillLevel1")
        }
        else if (soccerSkill.containsString("2")) {
            soccerSkillView.image = UIImage(named: "SkillLevel2")
        }
        else if (soccerSkill.containsString("3")) {
            soccerSkillView.image = UIImage(named: "SkillLevel3")
        }
        else if (soccerSkill.containsString("4")) {
            soccerSkillView.image = UIImage(named: "SkillLevel4")
        }
        else if (soccerSkill.containsString("5")) {
            soccerSkillView.image = UIImage(named: "SkillLevel5")
        }
        
        if (volleyballSkill.containsString("1")) {
            volleyballSkillView.image = UIImage(named: "SkillLevel1")
        }
        else if (volleyballSkill.containsString("2")) {
            volleyballSkillView.image = UIImage(named: "SkillLevel2")
        }
        else if (volleyballSkill.containsString("3")) {
            volleyballSkillView.image = UIImage(named: "SkillLevel3")
        }
        else if (volleyballSkill.containsString("4")) {
            volleyballSkillView.image = UIImage(named: "SkillLevel4")
        }
        else if (volleyballSkill.containsString("5")) {
            volleyballSkillView.image = UIImage(named: "SkillLevel5")
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func backPressed(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func changeProfilePic() {
        if (self.photosOptionView.alpha == 0) {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.photosOptionView.alpha = 1
            }
        }
        else {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.photosOptionView.alpha = 0
            }
        }
    }

    @IBAction func photoFromLibrary(sender:UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .PhotoLibrary
        presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func takePhoto(sender:UIButton) {
        picker.allowsEditing = true
        picker.sourceType = .Camera
        presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func signOut(sender:UIButton) {
        PFUser.logOut()
        let viewController = self.navigationController?.viewControllers[0]
        self.navigationController?.popToViewController(viewController!, animated: true)
    }
    
    @IBAction func showRatings(sender:UIButton) {
        if (self.ratingsView.alpha == 1) {
            return
        }
        
        self.ratingsButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
        self.gamesButton.setTitleColor(UIColor(red: 102/255, green: 179/255, blue: 227/255, alpha: 1.0), forState: UIControlState.Normal)
        UIView.animateWithDuration(0.25) { () -> Void in
            self.gamesView.alpha = 0
            self.ratingsView.alpha = 1
        }
    }
    
    @IBAction func showGames(sender:UIButton) {
        let currentUser = PFUser.currentUser()!
        let temp_gameIDs = currentUser["games"] as? NSArray
        if temp_gameIDs != nil {
            self.gameIDs = temp_gameIDs!
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.gamesTable.reloadData()
        }
        self.num_gamesButton.setTitle("\(self.gameIDs.count)", forState: UIControlState.Normal)
        
        if (self.gamesView.alpha == 1) {
            return
        }
        
        self.gamesButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
        self.ratingsButton.setTitleColor(UIColor(red: 102/255, green: 179/255, blue: 227/255, alpha: 1.0), forState: UIControlState.Normal)
        UIView.animateWithDuration(0.25) { () -> Void in
            self.ratingsView.alpha = 0
            self.gamesView.alpha = 1
        }
    }
    
    
    @IBAction func addFriend(sender:UIButton) {
        let objectID = self.user.objectId!
        let currentUser = PFUser.currentUser()!
        currentUser.addObject(objectID, forKey: "friends")
        currentUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if (success) {
                print("User \(objectID) has been added successfully to your friends.")
            }
            else {
                print("An error occurred adding \(objectID) to your friends.")
            }
        }
    }
    
    
    @IBAction func showFriends(sender:UIButton) {
        self.performSegueWithIdentifier("FriendsViewControllerSegue", sender: nil)
    }
    
    
    
    // MARK: - UITableView Delegate and DataSource Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameIDs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:GamesTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! GamesTableViewCell
        
        cell.gameImage.layer.cornerRadius = 25.0
        cell.gameImage.clipsToBounds = true
        
        for title in gameTitles {
            let query = PFQuery(className: title)
            query.whereKey("objectId", equalTo: gameIDs.objectAtIndex(indexPath.row))
            query.getObjectInBackgroundWithId(gameIDs.objectAtIndex(indexPath.row) as! String, block: { (game:PFObject?, error:NSError?) -> Void in
                if (error == nil) {
                    if (game != nil) {
                        cell.gameImage.image = UIImage(named: title.lowercaseString)
                        cell.gameType.text = title
                        cell.gameTime.text = game!["date"] as? String
                    }
                }
            })
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Did select \(gameIDs.objectAtIndex(indexPath.row))")
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! GamesTableViewCell
        tempTitle = cell.gameType.text!
        self.performSegueWithIdentifier("ShowUserGameSegue", sender: gameIDs.objectAtIndex(indexPath.row))
    }
    
    // MARK: - UIImage Resizer
    func resizeImage(image: UIImage, newSize: CGSize) -> (UIImage) {
        let newRect = CGRectIntegral(CGRectMake(0,0, newSize.width, newSize.height))
        let imageRef = image.CGImage
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
        
        CGContextConcatCTM(context, flipVertical)
        CGContextDrawImage(context, newRect, imageRef)
        
        let newImageRef = CGBitmapContextCreateImage(context)! as CGImage
        let newImage = UIImage(CGImage: newImageRef)
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.photosOptionView.alpha = 0
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismissViewControllerAnimated(true, completion: nil)
        let newImage = self.resizeImage(chosenImage, newSize: CGSize(width: 100.0, height: 100.0))
        self.profilePic.contentMode = .ScaleAspectFill
        self.profilePic.image = newImage
        
        let imageData = NSData(data: UIImagePNGRepresentation(newImage)!)
        let imageFile = PFFile(data: imageData)
        
        let currentUser = self.user
        currentUser["profilePic"] = imageFile
        currentUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if (success) {
                print("Saved profile pic to parse")
            }
            else {
                print("An error occurred")
            }
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.photosOptionView.alpha = 0
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ShowUserGameSegue") {
            let viewController = segue.destinationViewController as! GameViewController
            let objectID = sender as! String
            
            let query = PFQuery(className: tempTitle)
            do {
                let game = try query.getObjectWithId(objectID)
                
                let file = game["image"] as? PFFile
                if (file != nil) {
                    do {
                        let data = try file!.getData()
                        let image = UIImage(data: data)!
                        viewController.image = image
                    } catch _ {
                        print("An error occurred trying to get file data from parse")
                    }
                }
                
                viewController.gameTitle = tempTitle
                viewController.date = game["date"] as! String
                viewController.skill = game["skillLevel"] as! String
                viewController.objectID = objectID
                viewController.players = NSMutableArray(array: game["players"] as! NSArray)
                
                let location = game["location"] as! PFGeoPoint
                viewController.location = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            } catch _ {
                print("An error occurred trying to load data for game with specified objectID")
            }

        }
        else if (segue.identifier == "FriendsViewControllerSegue") {
            let viewController = segue.destinationViewController as! FriendsViewController
            viewController.friends = self.friends
        }
    }
}







