//
//  GameLocationViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 10/15/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class GameLocationViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIPickerViewDelegate {
    @IBOutlet var doneButton:UIButton!
    @IBOutlet var mapView:MKMapView!
    @IBOutlet var mapTypePickerView:UIPickerView!
    @IBOutlet var mapTypeButton:UIButton!
    
    var mapPoint:CLLocationCoordinate2D?
    var locationManager:CLLocationManager = CLLocationManager()
    let types = ["Standard", "Satellite", "Hybrid"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapTypePickerView.alpha = 0
        self.mapTypePickerView.delegate = self
        
        self.mapView.delegate = self
        self.mapView.mapType = MKMapType.Standard
        self.mapView.showsUserLocation = true
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        let dropPinTap:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: Selector("dropPinAtTouch:"))
        dropPinTap.minimumPressDuration = 0.5
        self.mapView.addGestureRecognizer(dropPinTap)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if (mapPoint != nil) {
            let annotation:MapPin = MapPin.init(coordinate: mapPoint!, title: "Big Dawg", subtitle: "test")
            self.mapView.addAnnotation(annotation)
        }
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocation = locations.last!
        
        let center = CLLocationCoordinate2D(latitude:location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func getUserLocation(sender:UIButton) {
        self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func doneButtonPressed(sender:UIButton) {
        if (mapPoint == nil) {
            let alert:UIAlertController = UIAlertController(title: "Woops", message: "You must drop a pin on the area where the game will be located.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            let viewController:CreateGameViewController = self.navigationController?.viewControllers[2] as! CreateGameViewController
            viewController.gameLocation = mapPoint!
            self.navigationController?.popToViewController(viewController, animated: true)
        }
    }
    
    func dropPinAtTouch(gestureRecognizer:UIGestureRecognizer) {
        for pin in self.mapView.annotations {
            self.mapView.removeAnnotation(pin)
        }
        
        let touchPoint:CGPoint = gestureRecognizer.locationInView(self.mapView)
        let touchMapCoordinate:CLLocationCoordinate2D = self.mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        mapPoint = touchMapCoordinate
        
        let annotation:MapPin = MapPin.init(coordinate: touchMapCoordinate, title: "Game Location", subtitle: "")
        self.mapView.addAnnotation(annotation)
    }
    
    @IBAction func mapTypePressed(sender:UIButton) {
        if (self.mapTypePickerView.alpha == 0) {
            self.doneButton.alpha = 0
            UIView.animateWithDuration(0.25) { () -> Void in
                self.mapTypePickerView.alpha = 1
            }
        }
        else {
            self.doneButton.alpha = 1
            UIView.animateWithDuration(0.25) { () -> Void in
                self.mapTypePickerView.alpha = 0
            }
        }
        
    }
    
    
    // MARK: - Picker Delegate and Data Source Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return types.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return types[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.mapTypeButton.setTitle(types[row], forState: UIControlState.Normal)
        self.resignAllPickerViews()
        
        if (types[row] == "Standard") {
            self.mapView.mapType = MKMapType.Standard
        }
        else if (types[row] == "Satellite") {
            self.mapView.mapType = MKMapType.Satellite
        }
        else if (types[row] == "Hybrid") {
            self.mapView.mapType = MKMapType.Hybrid
        }
        
        if (types[row] == "Satellite" || types[row] == "Hybrid") {
            self.mapTypeButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.mapTypePickerView.reloadAllComponents()
        }
        else {
            self.mapTypeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            self.mapTypePickerView.reloadAllComponents()
        }
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if (self.mapTypeButton.titleLabel?.text == "Standard" || self.mapTypeButton.titleLabel?.text == "Map Type") {
            let titleData = types[row]
            let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName:UIColor.blackColor()])
            return myTitle
        }
        else {
            let titleData = types[row]
            let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
            return myTitle
        }
    }
    
    func resignAllPickerViews() {
        UIView.animateWithDuration(0.25) { () -> Void in
            self.mapTypePickerView.alpha = 0
            self.doneButton.alpha = 1
        }
    }
    

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }

}
