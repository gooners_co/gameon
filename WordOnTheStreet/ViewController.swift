//
//  ViewController.swift
//  WordOnTheStreet
//
//
//
//  ViewController acts as a Log In ViewController.  Handles
//  user log in as well as user sign up.
//
//  More Shit has been added.
//
//
//  Created by Scott Russell on 10/14/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var username:UITextField!
    @IBOutlet var password:UITextField!
    @IBOutlet var backgroundImage:UIImageView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.username.delegate = self
        self.password.delegate = self
        
        self.view.sendSubviewToBack(self.backgroundImage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func logInButtonPressed(sender:UIButton) {
        if (self.username.text == "" || self.password.text == "") {
            let alert = UIAlertController(title: "Woops", message: "You must fill out all fields before signing in.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        PFUser.logInWithUsernameInBackground(self.username.text!, password: self.password.text!) { (user:PFUser?, error:NSError?) -> Void in
            if user != nil {
                print("Successful login of \(self.username.text).")
                self.performSegueWithIdentifier("MainMapViewControllerSegue", sender: nil)
            }
            else {
                let alert = UIAlertController(title: "Woops", message: "Invalid username or password.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func signUpButtonPressed(sender:UIButton) {
        self.performSegueWithIdentifier("SignUpViewControllerSegue", sender: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == self.username) {
            self.username.resignFirstResponder()
            self.password.becomeFirstResponder()
        }
        else if (textField == self.password) {
            self.password.resignFirstResponder()
            self.logInButtonPressed(UIButton.init())
        }
        
        return true
    }
}

