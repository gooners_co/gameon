//
//  FriendsTableViewCell.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 12/10/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {
    @IBOutlet var profilePic:UIImageView!
    @IBOutlet var firstLast:UILabel!
    @IBOutlet var username:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
