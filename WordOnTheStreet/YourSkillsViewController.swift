//
//  YourSkillsViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 10/29/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import Parse

class YourSkillsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var picker:UIPickerView!
    @IBOutlet var baseballSkillButton:UIButton!
    @IBOutlet var basketballSkillButton:UIButton!
    @IBOutlet var footballSkillButton:UIButton!
    @IBOutlet var soccerSkillButton:UIButton!
    @IBOutlet var volleyballSkillButton:UIButton!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var tag = 0
    
    let skillLevels = ["1, Noob", "2, Play for Fun", "3, Average", "4, Athletic", "5, Skilled"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.picker.alpha = 0
        self.picker.delegate = self
        self.picker.dataSource = self

        self.baseballSkillButton.tag = 0
        self.basketballSkillButton.tag = 1
        self.footballSkillButton.tag = 2
        self.soccerSkillButton.tag = 3
        self.volleyballSkillButton.tag = 4
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("resignAllPickerViews"))
        self.view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Button Actions
    @IBAction func skillButtonPressed(sender:UIButton) {
        if (self.picker.alpha == 1 && tag == sender.tag) {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.picker.alpha = 0
            }
        }
        else if (self.picker.alpha == 1 && tag != sender.tag) {
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.picker.alpha = 0
                }, completion: { (succeeded:Bool) -> Void in
                    UIView.animateWithDuration(0.25) { () -> Void in
                        self.picker.alpha = 1
                    }
            })
        }
        else if (self.picker.alpha == 0) {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.picker.alpha = 1
            }
        }
        
        tag = sender.tag
    }
    
    @IBAction func done(sender:UIButton) {
        for view in self.view.subviews {
            if (view.isKindOfClass(UIButton)) {
                let button = view as! UIButton
                if (button.titleLabel?.text == "Choose Skill") {
                    let alert = UIAlertController(title: "Woops", message: "You must choose a skill for each sport before continuing.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        }
        
        // Refer to http://stackoverflow.com/questions/31137385/save-current-users-message-list-in-parse-with-swift
        let currentUser = PFUser.currentUser()
        let skills = PFObject(className: "UserSkills")
        skills["baseballSkill"] = self.baseballSkillButton.titleLabel!.text!
        skills["basketballSkill"] = self.basketballSkillButton.titleLabel!.text!
        skills["footballSkill"] = self.footballSkillButton.titleLabel!.text!
        skills["soccerSkill"] = self.soccerSkillButton.titleLabel!.text!
        skills["volleyballSkill"] = self.volleyballSkillButton.titleLabel!.text!
        skills["user"] = currentUser!.objectId
        
        skills.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if (success) {
                print("Successfully added skills to parse")
                self.performSegueWithIdentifier("MainMapViewControllerSegue", sender: nil)
            }
            else {
                print("An error occurred")
            }
        }
    }
    

    // MARK: - Picker Delegate and Data Source Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return skillLevels.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return skillLevels[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch (tag) {
            case 0:     //Baseball
                self.baseballSkillButton.setTitle(self.skillLevels[row], forState: UIControlState.Normal)
                self.baseballSkillButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
                break
            case 1:     //Basketball
                self.basketballSkillButton.setTitle(self.skillLevels[row], forState: UIControlState.Normal)
                self.basketballSkillButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
                break
            case 2:     //Football
                self.footballSkillButton.setTitle(self.skillLevels[row], forState: UIControlState.Normal)
                self.footballSkillButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
                break
            case 3:     //Soccer
                self.soccerSkillButton.setTitle(self.skillLevels[row], forState: UIControlState.Normal)
                self.soccerSkillButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
                break
            case 4:     //Volleyball
                self.volleyballSkillButton.setTitle(self.skillLevels[row], forState: UIControlState.Normal)
                self.volleyballSkillButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/255, alpha: 1.0), forState: UIControlState.Normal)
                break
            default:
                break
        }
    }
    
    func resignAllPickerViews() {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.picker.alpha = 0
        }
    }

}
