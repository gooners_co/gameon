//
//  CreateGameViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 10/15/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import MapKit
import Parse
import EventKit

class CreateGameViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var typeOfGamePicker:UIPickerView!
    @IBOutlet var timeOfGamePicker:UIDatePicker!
    @IBOutlet var skillLevelPicker:UIPickerView!
    @IBOutlet var gamePhoto:UIImageView!
    @IBOutlet var gamePhoto2:UIImageView!
    @IBOutlet var photoOptionsView:UIView!
    
    @IBOutlet var typeButton:UIButton!
    @IBOutlet var locationButton:UIButton!
    @IBOutlet var timeButton:UIButton!
    @IBOutlet var skillButton:UIButton!
    @IBOutlet var createButton:UIButton!
    @IBOutlet var photoButton:UIButton!
    
    @IBOutlet var label1:UILabel!
    @IBOutlet var label2:UILabel!
    @IBOutlet var label3:UILabel!
    @IBOutlet var label4:UILabel!
    @IBOutlet var label5:UILabel!
    
    let typesOfGames = ["Baseball", "Basketball", "Football", "Soccer", "Volleyball"]
    let skillLevels = ["1, Noobs", "2, Play for Fun", "3, Average", "4, Athletic", "5, Skilled"]
    
    var gameType:String?
    var gameLocation:CLLocationCoordinate2D?
    var gameTime:NSString?
    var skillLevel:String?
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.typeOfGamePicker.dataSource = self
        self.typeOfGamePicker.delegate = self
        self.skillLevelPicker.dataSource = self
        self.skillLevelPicker.delegate = self
        
        self.typeOfGamePicker.alpha = 0
        self.timeOfGamePicker.alpha = 0
        self.skillLevelPicker.alpha = 0
        self.photoOptionsView.alpha = 0
        
        self.timeOfGamePicker.addTarget(self, action: Selector("datePickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("resignAllPickerViews"))
        self.view.addGestureRecognizer(tapGesture)
        
        picker.delegate = self
        
        self.view.sendSubviewToBack(self.gamePhoto)
        
        self.photoOptionsView.layer.cornerRadius = 25.0
        self.gamePhoto2.layer.cornerRadius = 60.0
        self.gamePhoto2.clipsToBounds = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Picker Delegate and Data Source Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView == self.typeOfGamePicker) {
            return typesOfGames.count
        }
        else if (pickerView == self.skillLevelPicker) {
            return skillLevels.count
        }
        else {
            return 3
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView == self.typeOfGamePicker) {
            return typesOfGames[row]
        }
        else if (pickerView == self.skillLevelPicker) {
            return skillLevels[row]
        }
        else {
            return "Blah"
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == self.typeOfGamePicker) {
            typeButton.setTitle(typesOfGames[row], forState: UIControlState.Normal)
            self.label1.textColor = UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0)
            self.typeButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0), forState: UIControlState.Normal)
            gameType = typesOfGames[row]
        }
        else if (pickerView == self.skillLevelPicker) {
            skillButton.setTitle(skillLevels[row], forState: UIControlState.Normal)
            self.label4.textColor = UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0)
            self.skillButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0), forState: UIControlState.Normal)
            skillLevel = String(row+1)
        }
    }
    
    func resignAllPickerViews() {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.typeOfGamePicker.alpha = 0
            self.timeOfGamePicker.alpha = 0
            self.skillLevelPicker.alpha = 0
            self.photoOptionsView.alpha = 0
            self.createButton.alpha = 1
        }
    }
    
    // MARK: - Button Actions
    @IBAction func cancelCreateGame(sender:UIButton) {
        let transition: CATransition = CATransition()
        let timeFunc : CAMediaTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.duration = 0.25
        transition.timingFunction = timeFunc
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    @IBAction func typeOfGamePressed(sender:UIButton) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.timeOfGamePicker.alpha = 0
            self.skillLevelPicker.alpha = 0
            self.photoOptionsView.alpha = 0
        }
        
        if (self.typeOfGamePicker.alpha == 0) {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.typeOfGamePicker.alpha = 1
                self.createButton.alpha = 0
            }
        }
        else {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.typeOfGamePicker.alpha = 0
                self.createButton.alpha = 1
            }
        }
    }
    
    @IBAction func locationOfGamePressed(sender:UIButton) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.typeOfGamePicker.alpha = 0
            self.timeOfGamePicker.alpha = 0
            self.skillLevelPicker.alpha = 0
            self.createButton.alpha = 1
        }
        
        self.performSegueWithIdentifier("GameLocationViewControllerSegue", sender: nil)
        self.label2.textColor = UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0)
        self.locationButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0), forState: UIControlState.Normal)
    }
    
    @IBAction func timeOfGamePressed(sender:UIButton) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.typeOfGamePicker.alpha = 0
            self.skillLevelPicker.alpha = 0
            self.photoOptionsView.alpha = 0
        }
        
        if (self.timeOfGamePicker.alpha == 0) {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.timeOfGamePicker.alpha = 1
                self.createButton.alpha = 0
            }
        }
        else {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.timeOfGamePicker.alpha = 0
                self.createButton.alpha = 1
            }
        }
    }
    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        let strDate = dateFormatter.stringFromDate(datePicker.date)
        timeButton.setTitle(strDate, forState: UIControlState.Normal)
        self.label3.textColor = UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0)
        self.timeButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0), forState: UIControlState.Normal)
        gameTime = strDate
    }
    
    @IBAction func skillLevelPressed(sender:UIButton) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.typeOfGamePicker.alpha = 0
            self.timeOfGamePicker.alpha = 0
            self.photoOptionsView.alpha = 0
        }
        
        if (self.skillLevelPicker.alpha == 0) {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.skillLevelPicker.alpha = 1
                self.createButton.alpha = 0
            }
        }
        else {
            UIView.animateWithDuration(0.5) { () -> Void in
                self.skillLevelPicker.alpha = 0
                self.createButton.alpha = 1
            }
        }
    }
    
    @IBAction func addOrTakePhoto(sender:UIButton) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.typeOfGamePicker.alpha = 0
            self.timeOfGamePicker.alpha = 0
            self.skillLevelPicker.alpha = 0
        }
        
        if (self.photoOptionsView.alpha == 0) {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.photoOptionsView.alpha = 1
                self.createButton.alpha = 0
            })
        }
        else {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.photoOptionsView.alpha = 0
                self.createButton.alpha = 1
            })
        }
    }
    
    @IBAction func photoFromLibrary(sender:UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .PhotoLibrary
        presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func takePhoto(sender:UIButton) {
        picker.allowsEditing = true
        picker.sourceType = .Camera
        presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func createGame(sender:UIButton) {
        self.createButton.enabled = false
        if (gameType == nil || gameTime == nil || skillLevel == nil || gameLocation == nil) {
            let alert = UIAlertController(title: "Woops", message: "Make sure you fill out the information before creating your game.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            self.createButton.enabled = true
            return
        }

        var game = PFObject(className:"Baseball")
        switch (gameType!) {
            case "Baseball":
                game = PFObject(className: "Baseball")
                break
            case "Basketball":
                game = PFObject(className: "Basketball")
                break
            case "Football":
                game = PFObject(className: "Football")
                break
            case "Soccer":
                game = PFObject(className: "Soccer")
                break
            case "Volleyball":
                game = PFObject(className: "Volleyball")
                break
            default:
                break
        }
        
        let location = PFGeoPoint(latitude: gameLocation!.latitude, longitude: gameLocation!.longitude)
        if (self.gamePhoto.image != nil) {
            let image = self.resizeImage(self.gamePhoto.image!, newSize: CGSize(width: self.gamePhoto.image!.size.width / 5, height: self.gamePhoto.image!.size.height / 5))
            let imageData = UIImagePNGRepresentation(image)
            let file = PFFile(data: imageData!)
            
            game.setValue(file, forKey: "image")
        }
        
        game.setValue(location, forKey: "location")
        game.setValue(gameTime, forKey: "date")
        game.setValue(skillLevel!, forKey: "skillLevel")
        game.addObject(PFUser.currentUser()!.objectId!, forKey: "players")
        
        let eventStore = EKEventStore()
        
        game.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if (success) {
                print("Successfully created game")
                
                self.createButton.enabled = true
                
                let currentUser = PFUser.currentUser()!
                currentUser.addObject(game.objectId!, forKey: "games")
                currentUser.saveInBackground()
                
                self.gamePhoto.removeFromSuperview()
                
                switch EKEventStore.authorizationStatusForEntityType(EKEntityType.Event) {
                case .Authorized:
                    let alert = UIAlertController(title: "Game On", message: "Would you like to add this game to your calendar?", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction) -> Void in
                        self.insertEvent(eventStore)
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: { (alertAction:UIAlertAction) -> Void in
                        self.navigationController?.popViewControllerAnimated(true)
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    break
                case .Denied:
                    eventStore.requestAccessToEntityType(EKEntityType.Event, completion: { (granted:Bool, error:NSError?) -> Void in
                        if (granted) {
                            let alert = UIAlertController(title: "Game On", message: "Would you like to add this game to your calendar?", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction) -> Void in
                                self.insertEvent(eventStore)
                            }))
                            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: { (alertAction:UIAlertAction) -> Void in
                                self.navigationController?.popViewControllerAnimated(true)
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else {
                            print("Access denied")
                        }
                    })
                    break
                case .NotDetermined:
                    eventStore.requestAccessToEntityType(EKEntityType.Event, completion: { (granted:Bool, error:NSError?) -> Void in
                        if (granted) {
                            let alert = UIAlertController(title: "Game On", message: "Would you like to add this game to your calendar?", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction) -> Void in
                                self.insertEvent(eventStore)
                            }))
                            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: { (alertAction:UIAlertAction) -> Void in
                                self.navigationController?.popViewControllerAnimated(true)
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else {
                            print("Access denied")
                        }
                    })
                    break
                default:
                    break
                }
            }
        }

    }
    
    
    func insertEvent(store:EKEventStore) {
        let calendars = store.calendarsForEntityType(EKEntityType.Event)
        
        for calendar in calendars {
            if calendar.title == "Important" {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM/dd/yy, hh:mm a"
                let startDate = dateFormatter.dateFromString(self.gameTime! as String)!
                let finishDate = startDate.dateByAddingTimeInterval(3600)
                
                let event = EKEvent(eventStore: store)
                event.calendar = calendar
                event.title = "Game On, \(self.gameType!) Game"
                event.startDate = startDate
                event.endDate = finishDate
                
                let alarm = EKAlarm.init(relativeOffset: -30*60)
                event.addAlarm(alarm)
                
                do {
                    try store.saveEvent(event, span: EKSpan.ThisEvent)
                    print("\n\nSaved event")
                    let alert = UIAlertController(title: "Game On", message: "This game has been added to your calendar", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Thanks", style: UIAlertActionStyle.Cancel, handler: { (alertAction:UIAlertAction) -> Void in
                        self.navigationController?.popViewControllerAnimated(true)
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                } catch _ {
                    print("An error occurred trying to save the event")
                }
            }
        }
    }
    
    
    // MARK: - UIImage Resizer
    func resizeImage(image: UIImage, newSize: CGSize) -> (UIImage) {
        let newRect = CGRectIntegral(CGRectMake(0,0, newSize.width, newSize.height))
        let imageRef = image.CGImage
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
        
        CGContextConcatCTM(context, flipVertical)
        CGContextDrawImage(context, newRect, imageRef)
        
        let newImageRef = CGBitmapContextCreateImage(context)! as CGImage
        let newImage = UIImage(CGImage: newImageRef)
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.gamePhoto.contentMode = .ScaleAspectFill
        self.gamePhoto.image = chosenImage
        self.gamePhoto2.contentMode = .ScaleAspectFill
        self.gamePhoto2.image = chosenImage
        self.resignAllPickerViews()
        dismissViewControllerAnimated(true, completion: nil)
        
        for aView in self.view.subviews {
            if (aView.isKindOfClass(UIVisualEffectView)) {
                aView.removeFromSuperview()
            }
        }
        
        self.label5.textColor = UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0)
        self.photoButton.setTitleColor(UIColor(red: 15/255, green: 134/255, blue: 25/192, alpha: 1.0), forState: UIControlState.Normal)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.gamePhoto.frame
        self.gamePhoto.alpha = 0.45
        self.view.addSubview(blurView)
        self.view.sendSubviewToBack(blurView)
        self.view.sendSubviewToBack(self.gamePhoto)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.resignAllPickerViews()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "GameLocationViewControllerSegue") {
            let viewController = segue.destinationViewController as! GameLocationViewController
            viewController.mapPoint = gameLocation
        }
    }

}
