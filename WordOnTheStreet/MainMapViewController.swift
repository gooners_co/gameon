//
//  MainMapViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 10/14/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Parse

class MainMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIPickerViewDelegate {
    
    @IBOutlet weak var mapView:MKMapView!
    @IBOutlet var baseballButton:UIButton!
    @IBOutlet var baseballLabelButton:UIButton!
    @IBOutlet var basketballButton:UIButton!
    @IBOutlet var basketballLabelButton:UIButton!
    @IBOutlet var footballButton:UIButton!
    @IBOutlet var footballLabelButton:UIButton!
    @IBOutlet var soccerButton:UIButton!
    @IBOutlet var soccerLabelButton:UIButton!
    @IBOutlet var volleyballButton:UIButton!
    @IBOutlet var volleyballLabelButton:UIButton!
    @IBOutlet var searchButton:UIButton!
    @IBOutlet var mapTypePickerView:UIPickerView!
    @IBOutlet var mapTypeButton:UIButton!
    @IBOutlet var description_locationButton:UILabel!
    @IBOutlet var description_profileButton:UILabel!
    @IBOutlet var description_createButton:UILabel!
    @IBOutlet var description_searchButton:UILabel!
    
    let allGames:NSMutableArray = NSMutableArray()
    let types = ["Standard", "Satellite", "Hybrid"]
    
    var locationManager:CLLocationManager = CLLocationManager()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchButton.setBackgroundImage(UIImage(named: "PickSportButton"), forState: UIControlState.Normal)
        
        self.baseballButton.alpha = 0
        self.baseballLabelButton.alpha = 0
        self.basketballButton.alpha = 0
        self.basketballLabelButton.alpha = 0
        self.footballButton.alpha = 0
        self.footballLabelButton.alpha = 0
        self.soccerButton.alpha = 0
        self.soccerLabelButton.alpha = 0
        self.volleyballButton.alpha = 0
        self.volleyballLabelButton.alpha = 0
        self.mapTypePickerView.alpha = 0
        
        self.mapView.delegate = self
        self.mapView.mapType = MKMapType.Standard
        self.mapView.showsUserLocation = true
        
        self.mapTypePickerView.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        let userTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("resignSportButtons"))
        self.view.addGestureRecognizer(userTap)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchButton.setBackgroundImage(UIImage(named: "PickSportButton"), forState: UIControlState.Normal)
        
        self.baseballButton.alpha = 0
        self.baseballLabelButton.alpha = 0
        self.basketballButton.alpha = 0
        self.basketballLabelButton.alpha = 0
        self.footballButton.alpha = 0
        self.footballLabelButton.alpha = 0
        self.soccerButton.alpha = 0
        self.soccerLabelButton.alpha = 0
        self.volleyballButton.alpha = 0
        self.volleyballLabelButton.alpha = 0
        
        //self.searchButton.setBackgroundImage(UIImage(named: "PickSportButton"), forState: UIControlState.Normal)
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("resignAllPickerViews"))
        self.view.addGestureRecognizer(tap)
    }
    
    
    // MARK: - Sport Actions/Methods
    @IBAction func pressMe(sender:UIButton) {
        if (self.baseballButton.alpha == 0) {
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.baseballButton.alpha = 1
                self.baseballLabelButton.alpha = 1
                }, completion: { finished in
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.basketballButton.alpha = 1
                    self.basketballLabelButton.alpha = 1
                    }, completion: { finished in
                    UIView.animateWithDuration(0.1, animations: { () -> Void in
                        self.footballButton.alpha = 1
                        self.footballLabelButton.alpha = 1
                        }, completion: { finished in
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.soccerButton.alpha = 1
                            self.soccerLabelButton.alpha = 1
                            }, completion: { finished in
                            UIView.animateWithDuration(0.1, animations: { () -> Void in
                                self.volleyballButton.alpha = 1
                                self.volleyballLabelButton.alpha = 1
                            })
                        })
                    })
                })
            })
        }
        else {
            self.resignSportButtons()
        }
    }
    
    func resignSportButtons() {
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.volleyballButton.alpha = 0
            self.volleyballLabelButton.alpha = 0
            }, completion: { finished in
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.soccerButton.alpha = 0
                    self.soccerLabelButton.alpha = 0
                    }, completion: { finished in
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.footballButton.alpha = 0
                            self.footballLabelButton.alpha = 0
                            }, completion: { finished in
                                UIView.animateWithDuration(0.1, animations: { () -> Void in
                                    self.basketballButton.alpha = 0
                                    self.basketballLabelButton.alpha = 0
                                    }, completion: { finished in
                                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                                            self.baseballButton.alpha = 0
                                            self.baseballLabelButton.alpha = 0
                                        })
                                })
                        })
                })
        })
    }
    
    @IBAction func loadBaseballGames(sender:UIButton) {
        self.resignSportButtons()
        self.searchButton.setBackgroundImage(UIImage(named: "baseball"), forState: UIControlState.Normal)
        
        self.getAllGamesFor("Baseball")
        
        /**
        let alert = UIAlertController(title: "Searching...", message: "Finding all baseball games in your area", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Thanks!", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        **/
    }
    
    @IBAction func loadBasketballGames(sender:UIButton) {
        self.resignSportButtons()
        self.searchButton.setBackgroundImage(UIImage(named: "basketball"), forState: UIControlState.Normal)
        
        self.getAllGamesFor("Basketball")
        
        /**
        let alert = UIAlertController(title: "Searching...", message: "Finding all basketball games in your area", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Thanks!", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        **/
    }
    
    @IBAction func loadFootballGames(sender:UIButton) {
        self.resignSportButtons()
        self.searchButton.setBackgroundImage(UIImage(named: "football"), forState: UIControlState.Normal)
        
        self.getAllGamesFor("Football")
        
        /**
        let alert = UIAlertController(title: "Searching...", message: "Finding all football games in your area", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Thanks!", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        **/
    }
    
    @IBAction func loadSoccerGames(sender:UIButton) {
        self.resignSportButtons()
        self.searchButton.setBackgroundImage(UIImage(named: "soccer"), forState: UIControlState.Normal)
        
        self.getAllGamesFor("Soccer")
        
        /**
        let alert = UIAlertController(title: "Searching...", message: "Finding all soccer games in your area", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Thanks!", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        **/
    }

    @IBAction func loadVolleyballGames(sender:UIButton) {
        self.resignSportButtons()
        self.searchButton.setBackgroundImage(UIImage(named: "volleyball"), forState: UIControlState.Normal)
        
        self.getAllGamesFor("Volleyball")
        
        /**
        let alert = UIAlertController(title: "Searching...", message: "Finding all volleyball games in your area", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Thanks!", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        **/
    }
    
    func getAllGamesFor(gameType:String) {
        for pin in self.mapView.annotations {
            self.mapView.removeAnnotation(pin)
        }
        
        let query = PFQuery(className: gameType)
        do {
            let games = try query.findObjects()
            for game in games {
                print("Game:\n\(game)")
                allGames.addObject(game)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM/dd/yy, hh:mm a"
                let gameDate = dateFormatter.dateFromString(game["date"] as! String)
                if (self.gameIsOverdue(gameDate!)) {
                    
                }
                
                let location = game["location"] as! PFGeoPoint
                let mapCoordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                let annotation:MapPin = MapPin(coordinate: mapCoordinate, title: "\(gameType) Game", subtitle: game["date"] as! String)
                annotation.objectID = game.objectId
                self.mapView.addAnnotation(annotation)
            }
        } catch _ {
            print ("An error occurred trying to retrieve all games")
        }
    }
    
    
    func gameIsOverdue(gameDate:NSDate) -> Bool {
        var overdue = false
        
        
        
        return overdue
    }
    
    
    // MARK: - IBAction Methods
    @IBAction func createGamePressed(sender:UIButton) {
        self.resignSportButtons()
        self.performSegueWithIdentifier("CreateGameViewControllerSegue", sender: nil)
    }
    
    @IBAction func myProfilePressed(sender:UIButton) {
        self.resignSportButtons()
        self.performSegueWithIdentifier("MyProfileViewControllerSegue", sender: nil)
    }
    
    @IBAction func mapTypePressed(sender:UIButton) {
        if (self.mapTypePickerView.alpha == 0) {
            UIView.animateWithDuration(0.25) { () -> Void in
                self.mapTypePickerView.alpha = 1
            }
        }
        else {
            UIView.animateWithDuration(0.25) { () -> Void in
                self.mapTypePickerView.alpha = 0
            }
        }
        
    }
    
    
    // MARK: - MapView Methods
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocation = locations.last!
        
        let center = CLLocationCoordinate2D(latitude:location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func findMyLocation(sender:UIButton) {
        self.locationManager.startUpdatingLocation()
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation.isKindOfClass(MapPin)) {
            var pinView = self.mapView.dequeueReusableAnnotationViewWithIdentifier("pin")
            if (pinView == nil) {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                pinView!.canShowCallout = true
            }
            
            let button:UIButton = UIButton(type: UIButtonType.DetailDisclosure)
            pinView?.rightCalloutAccessoryView = button
            
            return pinView
        }
        return nil
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let pin = view.annotation as! MapPin
        if control == view.rightCalloutAccessoryView {
            //print(view.annotation!.title) // annotation's title
            //print(view.annotation!.subtitle) // annotation's subttitle
            //print(pin.objectID) // annotations objectID
            self.performSegueWithIdentifier("GameViewControllerSegue", sender: pin)
        }
    }
    
    
    // MARK: - Picker Delegate and Data Source Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return types.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return types[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.mapTypeButton.setTitle(types[row], forState: UIControlState.Normal)
        self.resignAllPickerViews()
        
        if (types[row] == "Standard") {
            self.mapView.mapType = MKMapType.Standard
        }
        else if (types[row] == "Satellite") {
            self.mapView.mapType = MKMapType.Satellite
        }
        else if (types[row] == "Hybrid") {
            self.mapView.mapType = MKMapType.Hybrid
        }
        
        if (types[row] == "Satellite" || types[row] == "Hybrid") {
            self.description_createButton.textColor = UIColor.whiteColor()
            self.description_locationButton.textColor = UIColor.whiteColor()
            self.description_profileButton.textColor = UIColor.whiteColor()
            self.description_searchButton.textColor = UIColor.whiteColor()
            
            self.mapTypeButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            self.baseballLabelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.basketballLabelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.footballLabelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.soccerLabelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.volleyballLabelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            self.mapTypePickerView.reloadAllComponents()
        }
        else {
            self.description_createButton.textColor = UIColor.blackColor()
            self.description_locationButton.textColor = UIColor.blackColor()
            self.description_profileButton.textColor = UIColor.blackColor()
            self.description_searchButton.textColor = UIColor.blackColor()
            
            self.mapTypeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            
            self.baseballLabelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            self.basketballLabelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            self.footballLabelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            self.soccerLabelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            self.volleyballLabelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            
            self.mapTypePickerView.reloadAllComponents()
        }
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if (self.mapTypeButton.titleLabel?.text == "Standard" || self.mapTypeButton.titleLabel?.text == "Map Type") {
            let titleData = types[row]
            let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName:UIColor.blackColor()])
            return myTitle
        }
        else {
            let titleData = types[row]
            let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
            return myTitle
        }
    }
    
    func resignAllPickerViews() {
        UIView.animateWithDuration(0.25) { () -> Void in
            self.mapTypePickerView.alpha = 0
        }
    }
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "MyProfileViewControllerSegue") {
            let viewController = segue.destinationViewController as! MyProfileViewController
            
            let transition: CATransition = CATransition()
            let timeFunc : CAMediaTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.duration = 0.25
            transition.timingFunction = timeFunc
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            
            let currentUser = PFUser.currentUser()!
            viewController.user = currentUser
            viewController.username = currentUser.username!
            let first = currentUser["firstName"] as! String
            let last = currentUser["lastName"] as! String
            let fullName = first + " " + last
            viewController.name = fullName
            
            //print("getting all game id's")
            let gameIDs = currentUser["games"] as? NSArray
            if (gameIDs != nil) {
                viewController.gameIDs = gameIDs!
            }
            
            //print("getting all friends")
            let friends = currentUser["friends"] as? NSMutableArray
            if (friends != nil) {
                viewController.friends = friends!
            }
            
            //print("getting profile pic")
            let file = currentUser["profilePic"] as? PFFile
            if (file != nil) {
                do {
                    let data = try file!.getData()
                    let image = UIImage(data: data)
                    viewController.profileImage = image!
                } catch _ {
                    print("Error getting profile pic")
                }
            }
            
            self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
        }
        else if (segue.identifier == "CreateGameViewControllerSegue") {
            let transition: CATransition = CATransition()
            let timeFunc : CAMediaTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.duration = 0.25
            transition.timingFunction = timeFunc
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
            
            self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
        }
        else if (segue.identifier == "GameViewControllerSegue") {
            let viewController = segue.destinationViewController as! GameViewController
            
            let pin = sender as! MapPin
            var className = String()
            
            if (pin.title!.containsString("Baseball")) {
                className = "Baseball"
            } else if (pin.title!.containsString("Basketball")) {
                className = "Basketball"
            } else if (pin.title!.containsString("Football")) {
                className = "Football"
            } else if (pin.title!.containsString("Soccer")) {
                className = "Soccer"
            } else if (pin.title!.containsString("Volleyball")) {
                className = "Volleyball"
            }
            
            let query = PFQuery(className: className)
            query.whereKey("objectId", equalTo: pin.objectID!)
            do {
                let results = try query.findObjects()
                let game = results.last
                
                let file = game!["image"] as? PFFile
                if (file != nil) {
                    do {
                        let data = try file!.getData()
                        let image = UIImage(data: data)!
                        viewController.image = image
                    } catch _ {
                        print("An error occurred trying to get file data from parse")
                    }
                }
                
                viewController.gameTitle = className
                viewController.date = game!["date"] as! String
                viewController.skill = game!["skillLevel"] as! String
                viewController.objectID = pin.objectID!
                viewController.players = NSMutableArray(array: game!["players"] as! NSArray)
                
                let location = game!["location"] as! PFGeoPoint
                viewController.location = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            } catch _ {
                print("An error occurred trying to load data in segue to Game View Controller")
            }
        }
    }

}
