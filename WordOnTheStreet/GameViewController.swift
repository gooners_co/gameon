//
//  GameViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 11/6/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import Parse
import MapKit
import EventKit

class GameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var gameImage:UIImageView!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var dateLabel:UILabel!
    @IBOutlet var skillLabel:UILabel!
    @IBOutlet var playersTableView:UITableView!
    @IBOutlet var joinButton:UIButton!
    @IBOutlet var view_pickerView:UIView!
    @IBOutlet var pickerView:UIPickerView!
    @IBOutlet var view_submitButton:UIView!
    @IBOutlet var view_cancelButton:UIView!
    @IBOutlet var submitButton:UIButton!
    @IBOutlet var num_players:UIButton!
    
    var image = UIImage(named: "MissingGamePhoto")
    var gameTitle = String()
    var date = String()
    var skill = String()
    var location = CLLocationCoordinate2D()
    var objectID = String()
    var players = NSMutableArray()
    var playerRating = String()
    let skillLevels = ["1, Noob", "2, Play for Fun", "3, Average", "4, Athletic", "5, Skilled"]
    var submittedRating = String()
    var user = PFUser()
    var players_objectID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.playersTableView.delegate = self
        self.playersTableView.dataSource = self
        
        self.gameImage.layer.cornerRadius = 50.0
        self.gameImage.clipsToBounds = true
        
        self.gameImage.image = self.image
        self.titleLabel.text = self.gameTitle
        self.dateLabel.text = self.date
        self.skillLabel.text = self.skill
        
        self.view_pickerView.alpha = 0
        self.view_submitButton.alpha = 0
        self.view_cancelButton.alpha = 0
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.view_pickerView.layer.cornerRadius = 8.0
        self.view_pickerView.layer.masksToBounds = true
        self.view_submitButton.layer.cornerRadius = 8.0
        self.view_submitButton.layer.masksToBounds = true
        self.view_cancelButton.layer.cornerRadius = 8.0
        self.view_cancelButton.layer.masksToBounds = true
        
        if (players.containsObject(PFUser.currentUser()!.objectId!)) {
            self.joinButton.setTitle("Leave", forState: UIControlState.Normal)
            self.joinButton.setTitleColor(UIColor(red: 227/255, green: 45/255, blue: 42/255, alpha: 1.0), forState: UIControlState.Normal)
        }
        
        let query = PFQuery(className: "UserSkills")
        query.whereKey("user", equalTo: PFUser.currentUser()!.objectId!)
        do {
            let result = try query.getFirstObject()
            let phrase = "\(self.titleLabel.text!.lowercaseString)Skill"
            let temp = result[phrase] as! String
            self.playerRating = temp.substringToIndex(temp.startIndex.advancedBy(1))
            print ("\n\n\(self.playerRating)\n\n")
        } catch _ {
            print("Error trying to retrieve user's rating for \(self.titleLabel.text!).")
        }
        
        self.num_players.setTitle("\(self.players.count)", forState: UIControlState.Normal)
    }
    
    
    // MARK: - IBActions
    @IBAction func back(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func join(sender:UIButton) {
        //User wants to join game
        let query = PFQuery(className: gameTitle)
        query.whereKey("objectId", equalTo: objectID)
        if (self.joinButton.titleLabel!.text == "Join") {
            let rating = Int(self.playerRating)!
            if (rating >= Int(self.skillLabel.text!.substringToIndex(self.skillLabel.text!.startIndex.advancedBy(1)))) {
                print("\n\nThis person is ok to join the game\n\n")
                do {
                    let game = try query.getFirstObject()
                    game.addObject(PFUser.currentUser()!.objectId!, forKey: "players")
                    players.addObject(PFUser.currentUser()!.objectId!)
                    game.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                        if (success) {
                            print("Successfully added user into game")
                            let currentUser = PFUser.currentUser()!
                            currentUser.addObject(game.objectId!, forKey: "games")
                            currentUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                if (success) {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.playersTableView.reloadData()
                                        self.num_players.setTitle("\(self.players.count)", forState: UIControlState.Normal)
                                        UIView.animateWithDuration(0.25, animations: { () -> Void in
                                            self.joinButton.setTitle("Leave", forState: UIControlState.Normal)
                                            self.joinButton.setTitleColor(UIColor(red: 227/255, green: 45/255, blue: 42/255, alpha: 1.0), forState: UIControlState.Normal)
                                        })
                                    })
                                }
                            })
                        }
                    })
                } catch _ {
                    print("An error occurred adding the user into the game")
                }
                
                let eventStore = EKEventStore()
                
                switch EKEventStore.authorizationStatusForEntityType(EKEntityType.Event) {
                case .Authorized:
                    let alert = UIAlertController(title: "Game On", message: "Would you like to add this game to your calendar?", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction) -> Void in
                        self.insertEvent(eventStore)
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    break
                case .Denied:
                    print("Access denied")
                    break
                case .NotDetermined:
                    eventStore.requestAccessToEntityType(EKEntityType.Event, completion: { (granted:Bool, error:NSError?) -> Void in
                        if (granted) {
                            let alert = UIAlertController(title: "Game On", message: "Would you like to add this game to your calendar?", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction) -> Void in
                                self.insertEvent(eventStore)
                            }))
                            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else {
                            print("Access denied")
                        }
                    })
                    break
                default:
                    break
                }
            }
            else {
                let alert = UIAlertController(title: "Game On", message: "I'm sorry but you are not skilled enough to join this game", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Cancel, handler: { (alertAction:UIAlertAction) -> Void in
                    self.back(sender)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        else if (self.joinButton.titleLabel!.text == "Leave"){
            do {
                let game = try query.getFirstObject()
                game.removeObject(PFUser.currentUser()!.objectId!, forKey: "players")
                players.removeObject(PFUser.currentUser()!.objectId!)
                game.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                    if (success) {
                        print ("Successfully removed user's objectId from players array")
                        let currentUser = PFUser.currentUser()!
                        currentUser.removeObject(game.objectId!, forKey: "games")
                        currentUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                            if (success) {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.playersTableView.reloadData()
                                    self.num_players.setTitle("\(self.players.count)", forState: UIControlState.Normal)
                                    UIView.animateWithDuration(0.25, animations: { () -> Void in
                                        self.joinButton.setTitle("Join", forState: UIControlState.Normal)
                                        self.joinButton.setTitleColor(UIColor(red: 102/255, green: 179/255, blue: 227/255, alpha: 1.0), forState: UIControlState.Normal)
                                    })
                                })
                            }
                        })
                    }
                })
            } catch _ {
                print ("An error occurred removing user objectId from game's player array")
            }
        }
    }
    
    func insertEvent(store:EKEventStore) {
        let calendars = store.calendarsForEntityType(EKEntityType.Event) 
        
        for calendar in calendars {
            if calendar.title == "Important" {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM/dd/yy, hh:mm a"
                let startDate = dateFormatter.dateFromString(self.dateLabel.text!)!
                let finishDate = startDate.dateByAddingTimeInterval(3600)
                
                let event = EKEvent(eventStore: store)
                event.calendar = calendar
                event.title = "Word on the Street, \(self.titleLabel.text!) Game"
                event.startDate = startDate
                event.endDate = finishDate
                
                let alarm = EKAlarm.init(relativeOffset: -30*60)
                event.addAlarm(alarm)
                
                do {
                    try store.saveEvent(event, span: EKSpan.ThisEvent)
                    print("\n\nSaved event")
                    let alert = UIAlertController(title: "Game On", message: "This game has been added to your calendar", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Thanks", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                } catch _ {
                    print("An error occurred trying to save the event")
                }
            }
        }
    }
    
    
    @IBAction func rateButtonPressed(sender:UIButton) {
        let point = sender.convertPoint(CGPointZero, toView: self.playersTableView)
        let indexPath = self.playersTableView.indexPathForRowAtPoint(point)!
        self.players_objectID = players.objectAtIndex(indexPath.row) as! String
        print("Selected user object id from table view cell: \(self.players_objectID)")
        
        /**
            Do a UserSkills class query here so that it saves time changing the skill level for the particular game.
        **/
        
        self.submittedRating = self.skillLevels.first!
        
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.view_pickerView.alpha = 1
            self.view_submitButton.alpha = 1
            self.view_cancelButton.alpha = 1
        })
    }
    
    @IBAction func submitRating(sender:UIButton) {
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.view_pickerView.alpha = 0
            self.view_submitButton.alpha = 0
            self.view_cancelButton.alpha = 0
            
        })
        self.pickerView.reloadAllComponents()
        
        /**
            This is where I will get the users current skill level then use the new rating to create a new rating for the specified user.  Then save the data.
        **/
        let firstRating = Float(self.playerRating)!
        let secondRating = Float(self.submittedRating.substringToIndex(self.submittedRating.startIndex.advancedBy(1)))!

        let temp:Float = (firstRating + secondRating) / 2.0
        var rating:Int
        if (temp % 2 != 0) {
            rating = Int(temp) + 1
        }
        else {
            rating = Int(temp)
        }

        var newRating:String = ""
        switch (rating) {
        case 1:
            newRating = self.skillLevels[0]
            break
        case 2:
            newRating = self.skillLevels[1]
            break
        case 3:
            newRating = self.skillLevels[2]
            break
        case 4:
            newRating = self.skillLevels[3]
            break
        case 5:
            newRating = self.skillLevels[4]
            break
        default:
            if (rating > 5) {
                newRating = self.skillLevels[4]
            }
            else if (rating < 1) {
                newRating = self.skillLevels[0]
            }
            break
        }
        
        print("\nNew Rating: \(newRating)\n")
        let query = PFQuery(className: "UserSkills")
        query.whereKey("user", equalTo: self.players_objectID)
        do {
            let userSkills = try query.getFirstObject()
            let key = "\(self.titleLabel.text!.lowercaseString)Skill"
            userSkills[key] = newRating
            userSkills.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                if (success) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.playersTableView.reloadData()
                    })
                }
            })
        } catch _ {
            print("An error occurred in retrieving user \(self.players_objectID)'s skills")
        }
    }
    
    @IBAction func cancelRating(sender:UIButton) {
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.view_pickerView.alpha = 0
            self.view_submitButton.alpha = 0
            self.view_cancelButton.alpha = 0
        })
    }
    
    
    // MARK: - UITableView Delegate and DataSource Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:PlayersTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! PlayersTableViewCell
        
        cell.profilePic.layer.cornerRadius = 25.0
        cell.profilePic.clipsToBounds = true
        
        let query = PFUser.query()!
        query.whereKey("objectId", equalTo: players.objectAtIndex(indexPath.row))
        query.getObjectInBackgroundWithId((players.objectAtIndex(indexPath.row) as! String)) { (object:PFObject?, error:NSError?) -> Void in
            if (error == nil) {
                let user = object as! PFUser
                if (user.objectId == PFUser.currentUser()?.objectId) {
                    cell.rateButton.hidden = true
                }
                
                cell.username.text = user.username! as String
                
                let file = user["profilePic"] as? PFFile
                if (file != nil) {
                    do {
                        let data = try file!.getData()
                        let image = UIImage(data: data)!
                        cell.profilePic.image = image
                    } catch _ {
                        print("An error occurred trying to get file data for image from parse")
                    }
                }
            }
        }
        
        let skillsQuery = PFQuery(className: "UserSkills")
        skillsQuery.whereKey("user", equalTo: players.objectAtIndex(indexPath.row))
        skillsQuery.getFirstObjectInBackgroundWithBlock { (object:PFObject?, error:NSError?) -> Void in
            if (error == nil) {
                let skill = object!
                let text = self.gameTitle.lowercaseString + "Skill"
                cell.skillLevel.text = skill[text] as? String
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Did select \(players.objectAtIndex(indexPath.row))")
        self.performSegueWithIdentifier("ShowUserProfileSegue", sender: players.objectAtIndex(indexPath.row))
    }
    

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ShowUserProfileSegue") {
            let viewController = segue.destinationViewController as! MyProfileViewController
            let objectID = sender as! String
            
            /**
            let transition: CATransition = CATransition()
            let timeFunc : CAMediaTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.duration = 0.25
            transition.timingFunction = timeFunc
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            **/
            
            let query = PFUser.query()!
            query.whereKey("objectId", equalTo: objectID)
            do {
                let user = try query.getObjectWithId(objectID) as! PFUser
                viewController.user = user
                viewController.username = user.username!
                let first = user["firstName"] as! String
                let last = user["lastName"] as! String
                let fullName = first + " " + last
                viewController.name = fullName
                
                let gameIDs = user["games"] as? NSArray
                if (gameIDs != nil) {
                    viewController.gameIDs = gameIDs!
                }
                
                let friends = user["friends"] as? NSMutableArray
                if (friends != nil) {
                    viewController.friends = friends!
                }
                
                let file = user["profilePic"] as? PFFile
                if (file != nil) {
                    do {
                        let data = try file!.getData()
                        let image = UIImage(data: data)
                        viewController.profileImage = image!
                    } catch _ {
                        print("Error getting profile pic")
                    }
                }
                
            } catch _ {
                print("An error occurred querying the user with the given objectID")
            }
            
            //self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
        }
    }
    
    // MARK: - Picker Delegate and Data Source Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.skillLevels.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.skillLevels[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.submittedRating = self.skillLevels[row]
        print("\n\n\(self.submittedRating)\n\n")
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = self.skillLevels[row]
        let myTitle = NSAttributedString(string: titleData, attributes:[NSForegroundColorAttributeName:UIColor.whiteColor()])
        return myTitle
    }
    
    func resignAllPickerViews() {
        UIView.animateWithDuration(0.25) { () -> Void in
            self.pickerView.alpha = 0
        }
    }

}











