//
//  FriendsViewController.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 12/10/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit
import Parse

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView:UITableView!
    
    var friends = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func backPressed(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK: - Table View Delegate and DataSource Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Friends") as! FriendsTableViewCell
        
        cell.profilePic.layer.cornerRadius = 25.0
        cell.profilePic.layer.masksToBounds = true
        
        let query = PFUser.query()!
        query.whereKey("objectId", equalTo: self.friends.objectAtIndex(indexPath.row))
        do {
            let user = try query.getFirstObject() as! PFUser
            
            let first = user["firstName"] as! String
            let last = user["lastName"] as! String
            let full = "\(first) \(last)"
            cell.firstLast.text = full
            cell.username.text = user.username!
            
            let file = user["profilePic"] as? PFFile
            if (file != nil) {
                do {
                    let data = try file!.getData()
                    let image = UIImage(data: data)!
                    cell.profilePic.image = image
                } catch _ {
                    print("An error occurred trying to get file data for image from parse")
                }
            }
            
        } catch _ {
            print("An error occurred trying to retrieve info for \(self.friends.objectAtIndex(indexPath.row))")
        }
        
        return cell
    }
    

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
