//
//  PlayersTableViewCell.swift
//  WordOnTheStreet
//
//  Created by Scott Russell on 11/8/15.
//  Copyright © 2015 Scott Russell. All rights reserved.
//

import UIKit

class PlayersTableViewCell: UITableViewCell {
    @IBOutlet var profilePic:UIImageView!
    @IBOutlet var username:UILabel!
    @IBOutlet var skillLevel:UILabel!
    @IBOutlet var rateButton:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
